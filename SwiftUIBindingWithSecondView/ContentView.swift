////  ContentView.swift
//  SwiftUIBindingWithSecondView
//
//  Created on 16/11/2020.
//  
//

import SwiftUI

struct ContentView: View {
    
    @State private var isPlaying: Bool = true
    
    var body: some View {
       
        VStack {
            
            Text("Chaned Text")
                .font(.title)
                .padding(15)
                .foregroundColor(isPlaying ? .gray : .blue)
            
            ButtonPlay(isPlaying: $isPlaying)
            
        }
    }
}

struct ButtonPlay: View {
    
    @Binding var isPlaying: Bool
    
    var body: some View {
        Button(action: {isPlaying.toggle()}) {
            Text("Change color")
                .padding(20)
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
